import os
import sys
import logging
import configparser
from dotenv import load_dotenv
import instagrapi
from time import sleep
import datetime
from croniter import croniter

# Set up logger
logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', 
    level=logging.INFO)

# Load environment variables from .env file
load_dotenv()

# Access environment variables
username = os.getenv('USERNAME')
password = os.getenv('PASSWORD')

# Load configuration file
config = configparser.ConfigParser()
try:
    config.read('config/config.ini')
except configparser.Error as e:
    logging.error("Error reading config file: %s", e, exc_info=True)
    sys.exit(1)

# Checks if a string text contains ignore words 
def check_ignore_words(text):
    for word in text.split():
        if str(word).strip('#') in config['ignore']['words']:
            return True
    return False

# Checks if a username is ignored
def check_ignore_users(username):
    if username in config['ignore']['usernames']:
        return True
    return False

# Schedules a cron job
def run_job(cron_expression, job):
    now = datetime.datetime.now()
    cron = croniter(cron_expression, now)
    next_run_time = cron.get_next(datetime.datetime)
    while True:
        wait_time = (next_run_time - datetime.datetime.now()).total_seconds()
        if wait_time:
            sleep(wait_time)
        job()
        next_run_time = cron.get_next(datetime.datetime)

# The bot cycle
def like_cycle(api, ignored_words, ignored_user_ids):
    # Get a list of users I follow
    following = api.get_following(user_id=api.client.user_id, amount=config['general']['max_followed'])
    for followed in following:

        ignore = False

        # Check if user is ignored
        if followed in ignored_user_ids:
            ignore = True

        # Get a lits of the followed user's followers
        if not ignore:
            followers = api.get_followers(user_id=followed, amount=config['general']['max_followers'])
            for follower in followers:

                # Check if user is ignored
                if follower in ignored_user_ids:
                    ignore = True

                if not ignore:
                    # Check the user's bio for ignore words
                    for word in api.get_bio(user_id=follower).split():
                        if word.strip('#') in ignored_words:
                            logging.info("Found ignored word %s in bio for user %s. Skipping user..." % (word, follower))
                            ignore = True
                
                    if not ignore:
                        # Get a list of posts
                        posts = api.get_posts(user_id=follower, amount=10)
                        for post in posts:
                    
                            # Check the post's caption for ignore words
                            ignore = False
                            for word in post.caption_text.split():
                                if word.strip('#') in ignored_words:
                                    logging.info("Found ignored word %s in the caption for post %s. Skipping post..." % (word, post.id))
                                    ignore = True 
                            
                            if not ignore:
                                if api.like_post(post.id):
                                    logging.info("Success.")
                                else:
                                    logging.warning("Failed.")

                            # Sleep before next post
                            sleep(int(config['sleep']['posts']))

                    # Sleep before next user
                    sleep(int(config['sleep']['users']))
    return

# A class that initializes an instance of the instagrapi client
class InstagramAPI:
    def __init__(self, username, password):
        self.username = username
        self.password = password
        self.client = instagrapi.Client()
        try:
            self.client.login(username=self.username, password=self.password)
        except instagrapi.exceptions.FeedbackRequired as e:
            logging.error("Feedback required. Log in with a web browser and then restart this script: %s " % e, exc_info=True)
            sys.exit(0)
        except instagrapi.exceptions.SentryBlock as e:
            logging.error("Your IP is probably blocked: %s" % e, exc_info=True)
            sys.exit(0)
        except instagrapi.exceptions.BadPassword as e:
            logging.error("Bad password: %s" % e, exc_info=True)
            sys.exit(0)
        except instagrapi.exceptions.TwoFactorRequired as e:
            logging.error("Two-factor authentication is required and not provided: %s" % e, exc_info=True)
            sys.exit(0)
        except instagrapi.exceptions.PleaseWaitFewMinutes as e:
            logging.error("Try again later: %s" % e, exc_info=True)
            self.reconnect()
        except Exception as e:
            logging.error("Failed to log in: %s" % e, exc_info=True)
            self.reconnect()

    # A method which reconnects the Instagram client
    def reconnect(self):
        logging.info("Sleeping for %s seconds before reconnecting..." % config['sleep']['reconnect'])
        sleep(int(config['sleep']['reconnect']))
        self.__init__(username, password)
        return

    # A method which returns a list of a user's followers
    def get_followers(self, user_id, amount: int = 0) -> list[str]:
        try: 
            logging.info("Getting a list of followers for user %s..." % user_id)
            result = self.client.user_followers(user_id=user_id, amount=amount)
        except instagrapi.exceptions.LoginRequired as e:
            logging.error("Looks like we need to log in: %s" % e, exc_info=True)
            self.reconnect()
        return result

    # A method which returns a list of user_ids that a user is following
    def get_following(self, user_id, amount: int = 0) -> list[str]:
        try:
            logging.info("Getting a list of users that %s is following..." % user_id)
            result = self.client.user_following(user_id=user_id, amount=amount)
        except instagrapi.exceptions.LoginRequired as e:
            logging.error("Looks like we need to log in: %s" % e, exc_info=True)
            self.reconnect()
        return result

    # A method which returns a list of posts
    def get_posts(self, user_id, amount: int = 0) -> list[instagrapi.types.Media]:
        try:
            logging.info("Getting a list of posts by user %s..." % user_id)
            result = self.client.user_medias(user_id=user_id, amount=amount)
        except instagrapi.exceptions.LoginRequired as e:
            logging.error("Looks like we need to log in: %s" % e, exc_info=True)
            self.reconnect()
        return result

    # A method which returns the bio of a user as a string
    def get_bio(self, user_id) -> str:
        try:
            logging.info("Getting user %s's bio..." % user_id)
            user = self.client.user_info(user_id)
        except instagrapi.exceptions.LoginRequired as e:
            logging.error("Looks like we need to log in: %s" % e, exc_info=True)
            self.reconnect()
        return str(user.biography)

    # A method which likes a post
    def like_post(self, media_id) -> bool:
        try:
            logging.info("Liking post %s..." % media_id)
            result = self.client.media_like(media_id=media_id)
        except instagrapi.exceptions.LoginRequired as e:
            logging.error("Looks like we need to log in: %s" % e, exc_info=True)
            self.reconnect()
        return result

def main():
    api = InstagramAPI(username, password)

    # Build a list of ignored words
    ignored_words = config['ignore']['words']

    # Build a list of user_ids of ignored users
    ignored_user_ids = []
    if len(config['ignore']['users']):
        for ignored_username in config['ignore']['users']:
            ignored_user_ids.append(api.client.user_info_by_username(ignored_username))

    # Do not interact with my own posts
    ignored_user_ids.append(api.client.user_id)

    # Schedule the like cycle
    run_job(cron_expression = config['general']['cron_expression'], job = like_cycle(api, ignored_words, ignored_user_ids))

if __name__ == '__main__':
    main()