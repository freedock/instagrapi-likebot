# instagrapi-likebot
Logs into Instagram, likes some posts of users who follow the users you follow. 

## Installation
Have Docker installed.

## Usage
Create a file called `.env` with your Instagram user credentials, like this: 
```
USERNAME=foo
PASSWORD=secret
```

Start the script with `docker-compose up -d`. 

## Contributing
Yes, thanks. Submit a pull request. 

## License
This project uses the GNU General Public License license. See [LICENSE.md](LICENSE.md) for more details. 
